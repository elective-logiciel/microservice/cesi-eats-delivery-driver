import { Int } from "mssql";
import { mssql, sqlConfig } from "../services/mssql.service"
import { Banking_Details } from '../models/banking_details.model';
const sql = require('mssql')

const mssqlDB = new mssql();

export class banking_detailsData {

    public async GetBankingDetailsByUserId(id_user: Number) {
        const pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config);

        var request = pool.request();

        request.input('id_user', sql.Int, id_user);

        const res = (await request.query('SELECT Id_Banking_Details, Rib FROM Banking_Details WHERE Id_Users = @id_user'));

        pool.close();

        return res;
    }

    public async InsertBankingDetails(banking_details: Banking_Details) {
        const pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config);

        var request = pool.request();

        request.input('id_user', sql.Int, banking_details.id_user);
        request.input('rib', sql.NVarChar, banking_details.rib);

        await request.query('INSERT INTO Banking_Details (Rib, Id_Users) VALUES (@rib, @id_user);');

        pool.close();
    }

    public async UpdateRibsByUserId(banking_details: Banking_Details) {
        const pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config);

        var request = pool.request();

        request.input('id_user', sql.Int, banking_details.id_user);
        request.input('rib', sql.NVarChar, banking_details.rib);

        await request.query('UPDATE Banking_Details SET Rib = @rib WHERE Id_Users = @id_user;');

        pool.close();
    }

    public async DeleteRibByUserId(id_user: Number) {
        const pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config);

        var request = pool.request();

        request.input('id_user', sql.Int, id_user);

        await request.query('DELETE FROM Banking_Details WHERE Id_Users = @id_user;');

        pool.close();
    }
}