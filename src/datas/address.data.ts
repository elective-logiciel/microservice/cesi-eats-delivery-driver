import { Address } from "../models/address.model";
import { mssql, sqlConfig } from "../services/mssql.service"
const sql = require('mssql')

const mssqlDB = new mssql();

export class AddressData {
    public async GetAddressByUserId(id_user: number) {
        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, id_user)

        const res = await request.query('SELECT Id_Address, Street, Zip_Code, City FROM Address WHERE Id_Users = @id_user')

        pool.close()

        return res
    }

    public async InsertAddress(address: Address) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('street', sql.NVarChar, address.street)
        request.input('zip_code', sql.NVarChar, address.zip_code)
        request.input('city', sql.NVarChar, address.city)
        request.input('id_user', sql.Int, address.id_user)

        await request.query('INSERT INTO Address (Street, Zip_Code, City, Id_Users) VALUES (@street, @zip_code, @city, @id_user)')

        pool.close()
    }

    public async DeleteAddressById(id_address: number) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_address', sql.Int, id_address)

        await request.query('DELETE FROM Address WHERE Id_Address = @id_address')

        pool.close()
    }

    public async DeleteAllAddressByIdUser(id_user: number) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, id_user)

        await request.query('DELETE FROM Address WHERE Id_Users = @id_user')

        pool.close()
    }

    public async UpdateAddressStreetById(address: Address) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_address', sql.Int, address.id_address)
        request.input('street', sql.NVarChar, address.street)

        await request.query('UPDATE Address SET Street = @street WHERE Id_Address = @id_address')

        pool.close()
    }

    public async UpdateAddressZipCodeById(address: Address) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_address', sql.Int, address.id_address)
        request.input('zip_code', sql.NVarChar, address.zip_code)

        await request.query('UPDATE Address SET Zip_Code = @zip_code WHERE Id_Address = @id_address')

        pool.close()
    }

    public async UpdateAddressCityById(address: Address) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_address', sql.Int, address.id_address)
        request.input('city', sql.NVarChar, address.city)

        await request.query('UPDATE Address SET City = @city WHERE Id_Address = @id_address')

        pool.close()
    }
}