import { Router } from 'express';
import { UserData } from '../datas/user.data';
import { User, convertQueryResToUser, convertQueryResToUserList } from '../models/user.model';
import { returnCreated, returnDeleted, returnSuccess, returnUpdated } from '../errors/success';
import { Error404, Error409 } from '../errors/errors';

const userController = Router();
const MssqlDB = new UserData();

/**
 * Get delivery-driver by user id
 * @api {get} /delivery-driver/:id Request delivery-driver information
 * @apiName cesi-eats-delivery-driver
 * @apiGroup delivery-driver
 *
 * @apiParam {Number} id_user Users unique ID.
 *
 * @apiSuccess {Number} id_user Id user of the delivery-driver.
 * @apiSuccess {String} email Email of the delivery-driver.
 * @apiSuccess {String} name Name of the delivery-driver.
 * @apiSuccess {String} firstname Firstname of the delivery-driver.
 * @apiSuccess {Number} id_parrain Id user of the delivery-driver's parrain.
 */
userController.get('/:id_user(\\d+)', async(req, res, next)  =>{
    try {
        console.log('Request send:', req.originalUrl)

        const query_res = await MssqlDB.GetUserById(req.params.id_user)
        if (query_res.rowsAffected[0] === 0) {
            throw new Error404('User not found, this user does not exist.')
        }

        const user: User = convertQueryResToUser(query_res)

        const message: string = 'Get delivery driver by id'
        returnSuccess(res, user, message);

    } catch (err) {
        next(err)
    }
});

// Get all delivery-driver
userController.get('/all', async(req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const query_res = await MssqlDB.GetUsers()
        const user_list = convertQueryResToUserList(query_res)

        const message: string = 'Get all delivery drivers'
        returnSuccess(res, user_list, message);

    } catch (err) {
        next(err)
    }
});

// Add delivery-driver
userController.post('/add', async(req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User =  new User({
            email: req.body.email,
            password: req.body.password,
            name: req.body.name,
            first_name: req.body.first_name,
        })

        user.IsEmailValid()
        user.IsPasswordValid()
        user.IsNameValid()
        user.IsFirstNameValid()

        var user_exits = await MssqlDB.IsUserExitsByEmail(user)
        if (user_exits) {
            throw new Error409("Email already used, can not create user.");
        }

        await MssqlDB.InsertUser(user)

        const message: string = 'Add delivery driver'
        returnCreated(res, message);
        
    } catch (err) {        
        next(err)
    }
})

// Delete delivery-driver by user id
userController.delete('/delete/:id_user(\\d+)', async(req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        await MssqlDB.DeleteUserById(req.params.id_user)

        const message: string = 'Delete delivery driver by id'
        returnDeleted(res, message);

    } catch (err) {
        next(err)
    }
})

// Update delivery-driver email by user id
userController.put('/update/email', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            email: req.body.email,
        })

        user.IsIdUserValid()
        user.IsEmailValid()

        var user_exits = await MssqlDB.IsUserExitsByEmail(user)
        if (user_exits) {
            throw new Error409("Email already used, can not change user email.");
        }

        await MssqlDB.UpdateUserEmailById(user)

        const message: string = 'Update delivery driver email by id'
        returnUpdated(res, message);

    } catch (err) {
        next(err)
    }
})

// Update delivery-driver password by user id
userController.put('/update/password', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            password: req.body.password,
        })

        user.IsIdUserValid()
        user.IsPasswordValid()

        await MssqlDB.UpdateUserPasswordById(user)

        const message: string = 'Update delivery driver password by id'
        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

// Update delivery-driver name by user id 
userController.put('/update/name', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            name: req.body.name,
        })

        user.IsIdUserValid()
        user.IsNameValid()

        await MssqlDB.UpdateUserNameById(user)

        const message: string = 'Update delivery driver name by id'
        returnUpdated(res, message)
        
    } catch (err) {
        next(err)
    }
})

// Update delivery-driver first name by user id
userController.put('/update/first_name', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            first_name: req.body.first_name,
        })

        user.IsIdUserValid()
        user.IsFirstNameValid()

        await MssqlDB.UpdateUserFirstNameById(user)

        const message: string = 'Update delivery driver first_name by id'
        returnUpdated(res, message);

    } catch (err) {
        next(err)
    }
})

// Update delivery-driver suspended by user id
userController.put('/update/suspended', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            suspended: req.body.suspended,
        })

        user.IsIdUserValid()
        user.IsSuspendedValid()

        await MssqlDB.UpdateUserSuspendedById(user)
        
        const message: string = 'Update delivery driver suspended by id'
        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

// Update delivery-driver parrain by user id
userController.put('/update/parrain',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            id_parrain: req.body.id_parrain,
        })

        user.IsIdUserValid()
        user.IsIdParrainValid()

        await MssqlDB.UpdateIdParrainById(user)

        const message: string = 'Update delivery driver id_parrain by id'
        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

export { userController };